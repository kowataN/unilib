using UnityEngine;
using System.Collections;

public class SingletonBase<T> where T : class, new() {
    private static readonly T _instance = new T();
    public static T Inst { get { return _instance; } }
}
